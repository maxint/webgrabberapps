﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace XPathChecker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WebUtils.XPathClipper currentClip = new WebUtils.XPathClipper();

        public MainWindow()
        {
            InitializeComponent();
            this.urlTextBox.Text = "http://www.baidu.com";
            this.xPathTextBox.Text = "//img";
        }

        private void xPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ExcuteXPathQuery(false);
        }

        private async void ExcuteXPathQuery(bool forceUpdate = false)
        {
            var url = this.urlTextBox.Text;
            if (forceUpdate || currentClip.Source != url)
            {
                await Task.Run(() =>
                {
                    currentClip.Source = url;
                });
            }
            var xpath = xPathTextBox.Text;
            if (String.IsNullOrEmpty(xpath))
            {
                this.webBrowser.Navigate(url);
            }
            else
            {
                var html = "";
                var i = 0;
                foreach (var item in currentClip.QueryXPath(xpath))
                {
                    html += String.Format("<table><td>{0}: </td><td>{1}</td></table>", i++, item);
                }
                UpdateHTML(html);
            }
            this.xPathTextBox.Focus();
        }

        private string AddUtf8HtmlHeader(string html)
        {
            return "<html><head>" +
                "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">" +
                "</head><body>" + html + "</body></html>";
        }

        private void UpdateHTML(string html)
        {
            // WPF：很不给力的WebBrowser.NavigateToString
            // http://www.cnblogs.com/mgen/archive/2012/06/11/2545264.html
            //  1. 编码问题
            //  2. 相对路径问题
            html = string.IsNullOrEmpty(html) ? "[Empty]" : AddUtf8HtmlHeader(html);
            this.webBrowser.NavigateToString(html);
        }

        private void urlTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                UpdateHTML("Loading...");
                ExcuteXPathQuery(false);
            }
        }
    }
}
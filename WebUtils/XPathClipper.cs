﻿using System;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace WebUtils
{
    public class XPathClipper
    {
        private HtmlDocument htmlDoc = new HtmlDocument();
        private string source = null;

        /// <summary>
        /// 当前URL路径
        /// </summary>
        public string Source
        {
            get { return source; }
            set
            {
                if (value != source)
                {
                    source = value;
                    var html = WebGrabber.GetHtmlDocForChinese(source);
                    if (html != null)
                        htmlDoc.LoadHtml(html);
                }
            }
        }

        /// <summary>
        /// 根据XPath得到HTML片段
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<string> QueryXPath(string query)
        {
            try
            {
                var url = new Uri(source);
                var root = url.Scheme + "://" + url.Host;
                if (string.IsNullOrEmpty(query))
                {
                    return new List<string>() { WebGrabber.ConvertToAbsolutePath(htmlDoc.DocumentNode.InnerHtml, root) };
                }
                else
                {
                    var nodes = htmlDoc.DocumentNode.SelectNodes(query);
                    if (nodes != null)
                        return nodes.Select(n => WebGrabber.ConvertToAbsolutePath(n.OuterHtml, root));
                }
            }
            catch (System.Exception /*ex*/)
            {
            }
            return Enumerable.Empty<string>();
        }
    }
}
﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace WebUtils
{
    public class WebGrabber
    {
        /// <summary>
        /// Not used now
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string ConvertExtendedASCII(string html)
        {
            var str = new StringBuilder();
            foreach (char c in html)
            {
                if (Convert.ToInt32(c) > 127)
                {
                    str.Append("&#" + Convert.ToInt32(c) + ";");
                }
                else
                {
                    str.Append(c);
                }
            }
            return str.ToString();
        }

        public static Stream CopyStream(Stream originStream)
        {
            MemoryStream stream = new MemoryStream();
            originStream.CopyTo(stream);
            stream.Position = 0;
            return stream;
        }

        private static Regex rgCharset = new Regex(@"charset\s*=\s*""?\s*(?<charset>[^""]+)\s*""", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);

        public static string GetCharset(HttpWebResponse response, Stream stream)
        {
            var contentType = response.Headers["content-type"];
            if (contentType != null)
            {
                int idx = contentType.IndexOf("charset=");
                if (idx != -1)
                    return contentType.Substring(idx + 8);
            }

            var streamCopy = CopyStream(stream);
            using (var sr = new StreamReader(streamCopy, Encoding.GetEncoding(response.CharacterSet)))
            {
                var html = sr.ReadToEnd();
                var match = rgCharset.Match(html);
                Debug.Assert(!String.IsNullOrEmpty(html));
                if (match != null && match.Length > 0)
                    return match.Groups["charset"].Value;
            }

            return "gb2312";
        }

        public static string GetHtmlDocForChinese(string url)
        {
            try
            {
                var req = HttpWebRequest.Create(new Uri(url));
                req.Method = "GET";
                var response = req.GetResponse() as HttpWebResponse;
                using (var responseStream = response.GetResponseStream())
                {
                    var stream = CopyStream(responseStream);
                    string charset = GetCharset(response, stream);
                    stream.Position = 0;
                    using (var sr = new StreamReader(stream, Encoding.GetEncoding(charset)))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message.ToString());
                Console.WriteLine(e.StackTrace);
                return null;
            }
        }

        private static Regex rgRelativeToAbsolute = new Regex("(<[^>]*?)(src|href|action)=[\"']([^:]+?[\"'][^:>]*?>)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);
        private static Regex rgNoScheme = new Regex("(<[^>]*?)(src|href|action)=[\"']//(.*?[\"'].*?>)", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Multiline);

        public static string ConvertToAbsolutePath(string text, string absoluteUrl)
        {
            var res = rgNoScheme.Replace(text, "$1$2=\"http://$3");
            res = rgRelativeToAbsolute.Replace(res, "$1$2=\"" + absoluteUrl + "/$3");
            return res;
        }
    }
}
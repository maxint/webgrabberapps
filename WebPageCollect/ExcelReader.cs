﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Excel;

namespace WebPageCollect
{
    internal class PostMeta
    {
        public readonly string Title;
        public readonly string Year;
        public readonly string Month;
        public readonly string Day;
        public readonly string Url;

        public PostMeta(string tt, string date, string url)
        {
            Title = tt;
            Month = date.Substring(4, 2);
            Day = date.Substring(6, 2);
            Year = date.Substring(0, 4);
            Url = url;
        }
    }

    internal class Range
    {
        public readonly int Start;
        public readonly int End;

        public int Count
        {
            get { return End - Start; }
        }

        public Range(int start, int size)
        {
            Start = start;
            End = start + size;
        }
    }

    internal class MagzineMeta
    {
        public List<Range> Marketing { get; private set; }

        public Range Technology { get; private set; }

        public List<PostMeta> Posts { get; private set; }

        public MagzineMeta()
        {
            Marketing = new List<Range>();
            Posts = new List<PostMeta>();
        }

        public IEnumerable<PostMeta> Get(Range range)
        {
            return Posts.Skip(range.Start).Take(range.Count);
        }

        public void Add(List<PostMeta> posts)
        {
            var range = new Range(Posts.Count, posts.Count);
            if (Marketing.Count == 4)
                Technology = range;
            else
                Marketing.Add(range);
            Posts.AddRange(posts);
        }

        public bool IsValid()
        {
            return Marketing.Count == 4 &&
                Enumerable.All(Marketing, x => { return x.Count > 0; }) &&
                Technology != null &&
                Technology.Count > 0;
        }

        public void SaveAsJSON(string fileName)
        {
            var serizlizer = new System.Web.Script.Serialization.JavaScriptSerializer();
            var str = serizlizer.Serialize(this);
            File.WriteAllText(fileName, str);
        }
    }

    internal class ExcelReader
    {
        static public MagzineMeta Load(string fileName)
        {
            var posts = new List<PostMeta>();
            var data = new MagzineMeta();
            foreach (var workSheet in Workbook.Worksheets(fileName))
            {
                foreach (var row in workSheet.Rows)
                {
                    // valid item
                    if (row.Cells.Length >= 4 &&
                        Enumerable.All(row.Cells.Take(4), x => { return x != null; }))
                    {
                        var id = row.Cells[0].Value;
                        var date = row.Cells[1].Value;
                        var title = row.Cells[2].Text;
                        var url = row.Cells[3].Text;
                        posts.Add(new PostMeta(title, date, url));
                    }
                    else if (posts.Count > 0)
                    {
                        data.Add(posts);
                        posts = new List<PostMeta>();
                    }
                }
            }
            if (posts.Count > 0)
                data.Add(posts);

            if (data.IsValid())
                return data;
            else
                return null;
        }
    }
}
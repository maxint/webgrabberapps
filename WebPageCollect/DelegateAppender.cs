﻿namespace WebPageCollect
{
    public delegate void LogTextAppend(string text);

    internal class DelegateAppender : log4net.Appender.AppenderSkeleton
    {
        public LogTextAppend LogTextAppend;

        public DelegateAppender()
        {
            LogTextAppend = delegate { };
        }

        protected override void Append(log4net.Core.LoggingEvent loggingEvent)
        {
            LogTextAppend(RenderLoggingEvent(loggingEvent));
        }
    }
}
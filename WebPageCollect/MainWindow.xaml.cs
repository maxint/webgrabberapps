﻿using System.IO;
using System.Windows;
using log4net;

namespace WebPageCollect
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Post));

        public MainWindow()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            foreach (var appender in logger.Logger.Repository.GetAppenders())
            {
                if (appender.GetType() == typeof(DelegateAppender))
                {
                    (appender as DelegateAppender).LogTextAppend += this.UpdateLog;
                }
            }
            logger.Info("Application started");
        }

        private void UpdateLog(string text)
        {
            this.logTextBox.AppendText(text);
        }

        private void generatButton_Click(object sender, RoutedEventArgs e)
        {
            var path = this.excelPathTextBox.Text;
            var root = Path.GetDirectoryName(path);
            var postdir = Path.Combine(root, "posts");
            var mag = ExcelReader.Load(path);
            if (this.forceCheckBox.IsChecked == true)
                Directory.Delete(postdir, true);
            Post.DownloadAll(mag, postdir);
            mag.SaveAsJSON(Path.Combine(root, "mag.json"));
        }

        private void openExcelBtn_Click(object sender, RoutedEventArgs e)
        {
            var dlg = new System.Windows.Forms.OpenFileDialog();
            dlg.DefaultExt = ".xlsx";
            dlg.Filter = "Excel 2007|*.xlsx|Excel 2003|*.xls";
            if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.excelPathTextBox.Text = dlg.FileName;
                this.generatButton.IsEnabled = true;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;

namespace WebPageCollect
{
    internal class Post
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(Post));

        private static string Get(string url, string xpath)
        {
            var xpathClip = new WebUtils.XPathClipper();
            xpathClip.Source = url;
            return xpathClip.QueryXPath(xpath).First();
        }

        private static string GetXPath(string url)
        {
            var host = new Uri(url).Host.ToLower();
            var MAP = new Dictionary<string, string>()
            {
                //{ "tech.hexun.com", "//div[@id='artibody']" },
                //{ "tech.sina.com.cn", "//div[@id='artibody']" },
                //{ "www.leiphone.com", "//div[@class='pageCont lp-article-comView ']" },
                //{ "finance.qq.com", "//div[id='Cnt-Main-Article-QQ']"},
                //{ "nb.zol.com.cn", "//div[itemprop='articleBody']"},
                //{ "article.pchome.net", "//div[5]/div[1]/div[7]"},
                //{ "finance.ifeng.com", "//div[id='main_content']"},
                //{ "www.36kr.com", "//section[@class='article']"},
                //{ "", ""},
            };
            if (MAP.ContainsKey(host))
                return MAP[host];
            else
                return null;
        }

        private static void DownloadPosts(List<PostMeta> posts, Range range, string postDir)
        {
            for (int i = range.Start; i < range.End; i++)
            {
                var post = posts[i];
                var fileName = Path.Combine(postDir, (i + 1) + ".html");
                if (File.Exists(fileName))
                {
                    logger.InfoFormat("Skip existed post: {0}, {1}", post.Title, post.Url);
                    continue;
                }

                logger.InfoFormat("Processing: {0}, {1}", post.Title, post.Url);
                var xpath = GetXPath(post.Url);

                string header = String.Format("<head>\n" +
                    "<meta http-equiv=\"Content-type\" content=\"text/html; charset=UTF-8\" />\n" +
                    "<title>{0}</title>\n" +
                    "</head>\n", post.Title);

                string contents;
                if (xpath != null)
                {
                    contents = String.Format("<a href=\"{0}\">{1}</a>\n", post.Url, post.Title) + Get(post.Url, xpath);
                }
                else
                {
                    // frame
                    contents = String.Format("<iframe width=\"100%\" height=\"800px\" src=\"{0}\" />\n", post.Url);
                }
                if (!String.IsNullOrEmpty(contents))
                {
                    contents = String.Format("<html>\n{1}<body>\n{0}\n</body>\n</html>", contents, header);
                    File.WriteAllText(fileName, contents);
                }
                else
                {
                    logger.InfoFormat("Failed to download post");
                }
            }
        }

        public static void DownloadAll(MagzineMeta mag, string postDir)
        {
            Directory.CreateDirectory(postDir);
            for (int i = 0; i < mag.Marketing.Count; i++)
                DownloadPosts(mag.Posts, mag.Marketing[i], postDir);
            DownloadPosts(mag.Posts, mag.Technology, postDir);
        }
    }
}
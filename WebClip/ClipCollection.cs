﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace WebClip
{
    [DataContract]
    internal class ClipCollection
    {
        [DataMember(Name = "Items")]
        private List<ClipItem> clipItems = new List<ClipItem>();

        public List<ClipItem> Items
        {
            get { return clipItems; }
        }

        public IEnumerable<String> ClipNames
        {
            get { return from item in clipItems select item.Name; }
        }

        public int Count
        {
            get { return clipItems.Count; }
        }

        public ClipItem At(int index)
        {
            return clipItems[index];
        }

        public ClipItem NewClip()
        {
            var item = new ClipItem("[New]");
            clipItems.Add(item);
            return item;
        }

        public ClipItem NewClip(string name)
        {
            var item = new ClipItem(name);
            clipItems.Add(item);
            return item;
        }

        public ClipItem NewClip(string name, string urlPath, string query)
        {
            var item = new ClipItem(name, urlPath, query);
            clipItems.Add(item);
            return item;
        }

        public void RemoveAt(int index)
        {
            clipItems.RemoveAt(index);
        }

        public static ClipCollection Load(string fileName)
        {
            return SerializerUtils.Deserialize<ClipCollection>(fileName);
        }

        public void Save(string fileName)
        {
            SerializerUtils.Serialize(fileName, this);
        }
    }
}
﻿using System.Runtime.Serialization;
using System.Text;
using System.Xml;

namespace WebClip
{
    internal class SerializerUtils
    {
        public static T Deserialize<T>(string fileName) where T : class
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            using (XmlReader reader = new XmlTextReader(fileName))
            {
                return serializer.ReadObject(reader) as T;
            }
        }

        public static void Serialize<T>(string fileName, T obj)
        {
            DataContractSerializer serializer = new DataContractSerializer(typeof(T));
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                Encoding = Encoding.UTF8
            };
            using (XmlWriter writer = XmlTextWriter.Create(fileName, settings))
            {
                serializer.WriteObject(writer, obj);
            }
        }
    }
}
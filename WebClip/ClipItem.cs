﻿using System.Runtime.Serialization;

namespace WebClip
{
    [DataContract]
    internal class ClipItem
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember(Name = "Source")]
        private string source = null;

        private WebUtils.XPathClipper xpathCliper = new WebUtils.XPathClipper();

        public string Source
        {
            get { return source; }
            set
            {
                if (value != source)
                {
                    source = value;
                    if (string.IsNullOrEmpty(source))
                    {
                        htmlClip = null;
                    }
                    else
                    {
                        ProcessQuery();
                    }
                }
            }
        }

        [DataMember(Name = "Query")]
        private string query = null;

        public string XPathQuery
        {
            get { return query; }
            set
            {
                if (value != query)
                {
                    query = value;
                    ProcessQuery();
                }
            }
        }

        private bool processed = false;
        private string htmlClip = null;

        public string HtmlClip
        {
            get
            {
                if (!processed)
                    ProcessQuery();
                return htmlClip;
            }
        }

        public ClipItem(string name)
        {
            Name = name;
        }

        public ClipItem(string name, string source, string query)
        {
            Name = name;
            ProcessQuery(source, query);
        }

        public string ProcessQuery(string source, string query)
        {
            this.source = source;
            this.query = query;
            return ProcessQuery();
        }

        /// <summary>
        /// Process XPath query and regenerate HTML clip.
        /// </summary>
        /// <returns></returns>
        public string ProcessQuery()
        {
            xpathCliper.Source = source;
            htmlClip = "";
            foreach (var item in xpathCliper.QueryXPath(query))
                htmlClip += item;
            processed = true;
            return htmlClip;
        }
    }
}
﻿using System.Windows;
using System.Windows.Controls;

namespace WebClip
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ClipCollection collection = null;
        private ClipItem currentClip = null;
        private const string DATA_FILE = "data.xml";

        public MainWindow()
        {
            InitializeComponent();
        }

        private string AddUtf8HtmlHeader(string html)
        {
            return "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>" + html + "</body></html>";
        }

        private void UpdateHTML(string html)
        {
            // WPF：很不给力的WebBrowser.NavigateToString
            // http://www.cnblogs.com/mgen/archive/2012/06/11/2545264.html
            //  1. 编码问题
            //  2. 相对路径问题
            html = string.IsNullOrEmpty(html) ? "[Empty]" : AddUtf8HtmlHeader(html);
            this.webBrowser.NavigateToString(html);
        }

        private void ExcuteXPathQuery(bool forceUpdate = false)
        {
            if (forceUpdate)
            {
                currentClip.Source = null;
                currentClip.Source = this.urlTextBox.Text;
            }
            currentClip.XPathQuery = xPathTextBox.Text;
            UpdateHTML(currentClip.HtmlClip);
        }

        private void xPathTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ExcuteXPathQuery(false);
        }

        private void executeXPathButton_Click(object sender, RoutedEventArgs e)
        {
            ExcuteXPathQuery(false);
        }

        private void loadUrlButton_Click(object sender, RoutedEventArgs e)
        {
            ExcuteXPathQuery(true);
        }

        private void addClipButton_Click(object sender, RoutedEventArgs e)
        {
            currentClip = collection.NewClip(clipNameTextBox.Text);
            TreeViewItem newChild = new TreeViewItem();
            newChild.Header = currentClip.Name;
            treeView.Items.Add(newChild);
        }

        private void treeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            int index = treeView.Items.IndexOf(treeView.SelectedItem);
            if (index != -1)
            {
                currentClip = collection.At(index);
                clipNameTextBox.Text = currentClip.Name;
                urlTextBox.Text = currentClip.Source;
                xPathTextBox.Text = currentClip.XPathQuery;
                ExcuteXPathQuery(true);
            }
        }

        private void deleteClipButton_Click(object sender, RoutedEventArgs e)
        {
            int index = treeView.Items.IndexOf(treeView.SelectedItem);
            if (index != -1)
            {
                collection.RemoveAt(index);
                treeView.Items.RemoveAt(index);
            }
        }

        private void loadListButton_Click(object sender, RoutedEventArgs e)
        {
            LoadAndCreateListView();
        }

        private void saveListButton_Click(object sender, RoutedEventArgs e)
        {
            SerializerUtils.Serialize(DATA_FILE, collection);
        }

        private void reloadListButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadAllClips();
            UpdateHTML(ComposeAllClip());
        }

        private void LoadAndCreateListView()
        {
            collection = ClipCollection.Load(DATA_FILE);
            if (collection != null)
            {
                treeView.Items.Clear();
                // Add all item to the tree view
                foreach (var item in collection.Items)
                {
                    TreeViewItem newChild = new TreeViewItem();
                    newChild.Header = item.Name;
                    treeView.Items.Add(newChild);
                }
                if (collection.Items.Count > 0)
                    currentClip = collection.At(0);
            }
        }

        private void ReloadAllClips()
        {
            if (collection != null)
            {
                foreach (var item in collection.Items)
                {
                    item.ProcessQuery();
                }
            }
        }

        private string ComposeAllClip()
        {
            if (collection != null)
            {
                string html = "";
                foreach (var item in collection.Items)
                {
                    html += "<h1>" + item.Name + "</h1>";
                    html += "<p>" + item.HtmlClip + "</p><hr/>";
                }
                return html;
            }
            else
            {
                return null;
            }
        }
    }
}